import { compareArrays } from "../src/main";

test("undefined arrays", async () => {
  const firstArray = undefined;
  const secondArray = undefined;

  expect(compareArrays(firstArray, secondArray)).toBeTruthy();
});

test("empty arrays", async () => {
  const firstArray = [];
  const secondArray = [];

  expect(compareArrays(firstArray, secondArray)).toBeTruthy();
});

test("firstArray empty", async () => {
  const firstArray = [];
  const secondArray = [3, 2, 9, 7, 1, 13, 11];

  expect(compareArrays(firstArray, secondArray)).toBeFalsy();
});

test("secondArray empty", async () => {
  const firstArray = [1, 4, 3, 11, 23, 5];
  const secondArray = [];

  expect(compareArrays(firstArray, secondArray)).toBeFalsy();
});

test("firstArray includes secondArray", async () => {
  const firstArray = [11, 4, 15, 14, 6, 9, 10, 1];
  const secondArray = [11, 4, 15, 14, 6];

  expect(compareArrays(firstArray, secondArray)).toBeFalsy();
});

test("secondArray includes firstArray", async () => {
  const firstArray = [6, 14, 1, 9, 8];
  const secondArray = [6, 14, 1, 9, 8, 12, 3, 2, 6];

  expect(compareArrays(firstArray, secondArray)).toBeFalsy();
});

test("same elements in different order", async () => {
  const firstArray = [15, 7, 11, 8, 3, 9, 5, 6];
  const secondArray = [11, 7, 5, 9, 6, 15, 3, 8];

  expect(compareArrays(firstArray, secondArray)).toBeFalsy();
});

test("equals arrays", async () => {
  const firstArray = [1, 15, 8, 7, 11, 6, 12, 9];
  const secondArray = [1, 15, 8, 7, 11, 6, 12, 9];

  expect(compareArrays(firstArray, secondArray)).toBeTruthy();
});
