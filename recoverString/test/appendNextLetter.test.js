import { appendNextLetter } from "../src/main";

test("undefined triplets array", async () => {
  const EXPECTED_RESULT = [];

  const currentResult = [];
  const triplets = undefined;

  expect(appendNextLetter(triplets, currentResult)).toStrictEqual(EXPECTED_RESULT);
});

test("undefined currentResult array", async () => {
  const EXPECTED_RESULT = ['w'];

  const currentResult = undefined;
  const triplets = [
    ["t", "u", "p"],
    ["w", "h", "i"],
    ["t", "s", "u"],
    ["a", "t", "s"],
    ["h", "a", "p"],
    ["t", "i", "s"],
    ["w", "h", "s"],
  ];

  expect(appendNextLetter(triplets, currentResult)).toStrictEqual(EXPECTED_RESULT);
});

test("empty triplets array", async () => {
  const EXPECTED_RESULT = [];

  const currentResult = [];
  const triplets = [];

  expect(appendNextLetter(triplets, currentResult)).toStrictEqual(EXPECTED_RESULT);
});

test("first letter", async () => {
  const EXPECTED_RESULT = ['w'];

  const currentResult = [];
  const triplets = [
    ["t", "u", "p"],
    ["w", "h", "i"],
    ["t", "s", "u"],
    ["a", "t", "s"],
    ["h", "a", "p"],
    ["t", "i", "s"],
    ["w", "h", "s"],
  ];

  expect(appendNextLetter(triplets, currentResult)).toStrictEqual(EXPECTED_RESULT);
});

test("last letter", async () => {
  const EXPECTED_RESULT = ['w', 'h', 'a', 't', 'i', 's', 'u', 'p'];

  const currentResult = ['w', 'h', 'a', 't', 'i', 's', 'u'];
  const triplets = [
    ["t", "u", "p"],
    ["w", "h", "i"],
    ["t", "s", "u"],
    ["a", "t", "s"],
    ["h", "a", "p"],
    ["t", "i", "s"],
    ["w", "h", "s"],
  ];

  expect(appendNextLetter(triplets, currentResult)).toStrictEqual(EXPECTED_RESULT);
});

test("letter in the middle", async () => {
  const EXPECTED_RESULT = ['w', 'h', 'a', 't', 'i'];

  const currentResult = ['w', 'h', 'a', 't'];
  const triplets = [
    ["t", "u", "p"],
    ["w", "h", "i"],
    ["t", "s", "u"],
    ["a", "t", "s"],
    ["h", "a", "p"],
    ["t", "i", "s"],
    ["w", "h", "s"],
  ];

  expect(appendNextLetter(triplets, currentResult)).toStrictEqual(EXPECTED_RESULT);
});

test("no more letters", async () => {
  const EXPECTED_RESULT = ['w', 'h', 'a', 't', 'i', 's', 'u', 'p'];

  const currentResult = ['w', 'h', 'a', 't', 'i', 's', 'u', 'p'];
  const triplets = [
    ["t", "u", "p"],
    ["w", "h", "i"],
    ["t", "s", "u"],
    ["a", "t", "s"],
    ["h", "a", "p"],
    ["t", "i", "s"],
    ["w", "h", "s"],
  ];;

  expect(appendNextLetter(triplets, currentResult)).toStrictEqual(EXPECTED_RESULT);
});