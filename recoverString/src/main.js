const appendNextLetter = (triplets = [], currentResult = []) => {
  const result = [...currentResult];

  let nextLetter = undefined;

  for (const triplet of triplets) {
    let index = 0;

    while (currentResult.includes(triplet[index])) {
      index++;
    }

    if (index < triplet.length) {
      for (const _triplet of triplets) {
        if (nextLetter) {
          let _index = 0;

          while (currentResult.includes(_triplet[_index])) {
            _index++;
          }

          if (
            !currentResult.includes(nextLetter) &&
            _triplet.indexOf(nextLetter) > _index
          ) {
            nextLetter = _triplet[_index];
          }
        } else {
          nextLetter = triplet[index];
        }
      }
    }
  }

  if (nextLetter) {
    result.push(nextLetter);
  }

  return result;
};

const compareArrays = (firstArray = [], secondArray = []) =>
  firstArray.length === secondArray.length &&
  firstArray.filter((element, index) => secondArray[index] !== element)
    .length === 0;

const recoverSecret = (triplets = []) => {
  let currentResult = [];
  let end = false;

  while (!end) {
    const result = appendNextLetter(triplets, currentResult);

    end = compareArrays(result, currentResult);
    currentResult = result;
  }

  return currentResult.join("");
};

module.exports = {
  compareArrays,
  appendNextLetter,
  recoverSecret,
};
